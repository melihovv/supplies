$(document).ready(function () {
    var brand = $('#brandsSelect');
    var type = $('#typesSelect');
    var model = $('#modelsSelect');

    brand.change(function () {
        model.val('');

        if (type.val() !== '') {
            loadModels();
        }
    });

    type.change(function () {
        model.val('');

        if (brand.val() !== '') {
            loadModels();
        }
    });

    $('#suppliesFilter').on('submit', function(e){
        e.preventDefault();
        
        if (brand.val() !== '' && type.val() !== '' && model.val() !== '') {
            $.ajax('/supplies/' + model.val())
                .done(function (html) {
                    $('#error').text('');
                    $('#content').empty().append(html);
                })
                .fail(function () {
                    $('#error').text('Невозможно получить список расходных материалов');
                });
        }
    });

    function loadModels() {
        $.ajax('/models/' + brand.val() + '/' + type.val())
            .done(function (data) {
                $('#error').text('');

                var items = JSON.parse(data);
                var options = '<option value="">Выберите модель</option>';
                items.forEach(function (item) {
                    options += '<option value="' + item.id + '">' + item.model + '</option>';
                });

                model.empty().append(options);
            })
            .fail(function () {
                $('#error').text('Невозможно получить список моделей');
            });
    }
});