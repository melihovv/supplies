module.exports = function (productModel) {
    return {
        index: function (req, res) {
            res.render('index', {title: 'Подбор расходных материалов для принтеров, МФУ, сканнеров'});
        },

        getModels: function (req, res) {
            productModel.getAllModels(req.params.model, req.params.type, function (error, models) {
                if (error) {
                    console.error(error);
                    return res.status(500).send('Невозможно получить список моделей');
                }

                res.json(JSON.stringify(models));
            });
        },

        getSupplies: function (req, res) {
            productModel.getSupplies(req.params.model, function (error, supplies) {
                if (error) {
                    console.error(error);
                    return res.status(500).send('Невозможно получить список комплектующих');
                }

                var result = '';
                supplies.forEach(function (supply) {
                    result += '<p>' + supply.name + ' ' + supply.model + '</p>';
                });

                res.send(result);
            });
        }
    };
};
