module.exports = function (pool) {
    return {
        getAllModels: function (manufacturer_id, type_id, cb) {
            pool.query('SELECT id,model FROM products WHERE manufacturer_id=? AND type_id=?', [manufacturer_id, type_id], cb);
        },
        
        getSupplies: function (product_id, cb) {
            pool.query('select p.model, t.name from products p, types t, relations r where p.id=r.supply_id and r.product_id=? and p.type_id=t.id', [product_id], cb);
        }
    };
};
